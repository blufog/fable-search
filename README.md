## Fable-search
Search microservice for Fable

### Requirements
 * `virtualenv` on the host machine
 * `build-essentials`
 * `requests==2.12.4`
 * `falcon==1.1.0`
 * `fuzzywuzzy==0.14.0`
 * `py2neo==3.1.2`
 * `gunicorn==19.6.0`
 * `python-Levenshtein==0.12.0`

### Usage
 * `make init` will make a new virtual environment in the `venv` directory,
   installing required dependencies
 * `make start` will start the service at the address and port specified in the
   Makefile
 * `make dev` will start the service at the specified URI, set to scan for
   changes in code and reload accordingly

## Configuration
 * See `search/conf.py` for some stuff to fiddle with
