# For external interface
LISTEN_ADDRESS=0.0.0.0
#LISTEN_ADDRESS=127.0.0.1
# Change this to anything
LISTEN_PORT=8000
init:
	pip install -r requirements.txt

start:
	cd search && gunicorn search:api -b $(LISTEN_ADDRESS):$(LISTEN_PORT)

dev:
	cd search && gunicorn search:api --reload -b $(LISTEN_ADDRESS):$(LISTEN_PORT)
