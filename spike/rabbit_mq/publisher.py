#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pika, os, logging

logging.basicConfig()

# Parse CLOUDAMPQ_URL (Fallback to localhost)

url = os.environ.get('CLOUDAMQP', 'amqp://guest:guest@localhost/%2f')

params = pika.URLParameters(url)
params.socket_timeout = 5

connection = pika.BlockingConnection(params)
channel = connection.channel() # Start 
channel.queue_declare(queue='pdfprocess') # Declare a queue

# Send a message

channel.basic_publish(exchange='', routing_key='pdfprocess', body='User\
                      information')
print(" [*] Send message")
connection.close()
