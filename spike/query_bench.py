import time
from py2neo import Graph

graph = Graph(user = "neo4j", password="neo4j")

t0 = time.time()

result = graph.run("MATCH (n) WHERE ID(n) = 13 return labels(n)")

t1 = time.time()

print("Time taken for query : ", t1 - t0)
