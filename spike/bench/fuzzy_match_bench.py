#!/usr/bin/env python
# -*- coding: utf-8 -*-

# TODO: Bench the string matching algorithm 
# FIXME: It's slow

from fuzzywuzzy import process
import time
import random

NUM_ELEMENTS = 1000
NUM_SEARCHES = 100

names = []
with open('./name_data') as f:
    for line in f:
        names.append(line)

test_data = names[:NUM_ELEMENTS]

t0 = time.time()

for i in range(NUM_SEARCHES):
    process.extract(random.choice(names), test_data, limit=3)

t1 = time.time()
print("Time taken : ", ( t1 - t0 )*1000, "ms")
