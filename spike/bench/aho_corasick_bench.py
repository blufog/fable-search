#!/usr/bin/env python
# -*- coding: utf-8 -*-

# TODO: Bench the string matching algorithm 

import ahocorasick
import time
import random

NUM_ELEMENTS = 2000
NUM_SEARCHES = 1000

names = []
with open('./name_data') as f:
    for line in f:
        names.append(line)

test_data = names[:NUM_ELEMENTS]

A = ahocorasick.Automaton()
for idx, key in enumerate(names):
    A.add_word(key, (idx, key))

A.make_automaton()

t0 = time.time()

for i in range(NUM_SEARCHES):
    list(A.keys(random.choice(test_data) + "?", "?",
                ahocorasick.MATCH_AT_LEAST_PREFIX))


t1 = time.time()
print("Time taken : ", ( t1 - t0 )*1000, "ms")
