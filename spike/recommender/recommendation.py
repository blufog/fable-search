#!/usr/bin/env python3
# coding=utf-8
import logging

from db import db_instance as connection

def gen_path_query(id1, id2):
    id1 = str(id1)
    id2 = str(id2)
    return "MATCH (a), (b), p=shortestPath((a)-[*]-(b)) WHERE ID(a) = " + id1 +\
            " AND ID(b) = " + id2 + \
            " RETURN DISTINCT EXTRACT(n in nodes(p) | ID(n))"

class connectivity_model:
    """Stores information about the connectivity among the nodes in the graph"""

    def __init__(self):
        # The number of hops needed to go from one node to the other
        self.hops = {}
        self.update_model()

    def update_model(self):
        src_list = []
        result = connection.graph.run("MATCH (n) RETURN ID(n)")

        for record in result:
            src_list.append(record[0])
        dest_list = list(src_list)
        for src in src_list:
            self.hops[src] = {}

        for src in src_list:
            for dest in dest_list:
                if dest not in self.hops[src]:
                    result = connection.graph.run(gen_path_query(src, dest))
                    try:
                        node_list = result.next()[0]
                        # Now set all the distances we can get from this list
                        for id1, node1 in enumerate(node_list):
                            for id2, node2 in enumerate(node_list[id1:]):
                                self.hops[node1][node2] = id2
                                self.hops[node2][node1] = id2

                    except StopIteration:
                        self.hops[src][dest] = -1
                        self.hops[dest][src] = -1
            del dest_list[0]

relations = connectivity_model()
