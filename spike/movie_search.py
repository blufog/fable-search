#!/usr/bin/env python
# coding=utf-8
from py2neo import Graph, authenticate

movie_graph = Graph(password="neo4j")

print("Search engine test")

def prefix_match(key):
    return '\"(?i)' + key + '.*\"'

def suffix_match(key):
    return '\"(?i).*' + key + '\"';

def substring_match(key):
    return '\"(?i).*' + key + '.*\"'

while True:
    print("Enter the search query : ")
    query = str(input())

    query_text = "MATCH (a:Person) WHERE a.name =~ " + \
                substring_match(query) + " RETURN a.name"
    people = movie_graph.data(query_text)
    print("The people who matched : ")
    for record in people:
        print(record['a.name'])
    query_text = "MATCH (a:Movie) WHERE a.title =~ " + \
                 substring_match(query) + " RETURN a.title"
    movies = movie_graph.data(query_text)
    print("The movies which matched : ")
    for movie in movies:
        print(movie['a.title'])

