#!/usr/bin/env python3
# coding=utf-8
import py2neo
import logging

from conf import configuration as cfg
from time import sleep
import log
"""
Database access for the search service
"""
class db:
    def __init__(self):
        connected = False
        while not connected:
            try:
                self.graph = py2neo.Graph(cfg.DATABASE_URI, user=cfg.DATABASE_USERNAME,
                                   password=cfg.DATABASE_PASSWORD)
                connected = True
            except py2neo.packages.httpstream.http.SocketError:
                logging.warn("Failed to connect to the database, retrying..")
            sleep(0.5)


db_instance = db()
