#!/usr/bin/env python3
# coding=utf-8

import heapq

from ahocorasick import Automaton, MATCH_AT_LEAST_PREFIX

from db import db_instance as connection 

""" Module for tf-idf matching of query keywords """

class string_matcher:

    """Class to perform string matching and index for the search"""
    def __init__(self):
        """Initialize the string matcher"""
        # Model for the string matching
        self.models = [{'name':'PA',
                       'attributes':['first_name', 'last_name', 'profession',
                                     'location']},
                      {'name':'PE',
                       'attributes':['project_name', 'production_type']},
                      {'name':'BA',
                       'attributes':['business_name', 'location']}
                     ]

        # Common words not to be added to the trie
        self.stoplist = ['I', 'a', 'about', 'an', 'are', 'as', 'at', 'be', 'by', 
                         'com', 'for', 'from', 'how', 'in', 'is', 'it', 'of', 
                         'on', 'or', 'that', 'the', 'this', 'to', 'was', 'what', 
                         'when', 'where', 'who', 'will', 'with', 'the', 'www',
                         '']

        # For repeating words common accross nodes (document frequency)
        self.countkeeper = {}
        # Score for each node
        self.scorekeeper = {}
        self.matcher = Automaton()
        # Update the matcher on initialization
        self.update_matcher()
        # Some configuration constants
        self.MATCH_EXACT = 60 
        self.SCORE_CUTOFF = 1
        self.NUM_RESULTS = 20
        self.PREFIX_LIMIT = 3

    def gen_db_query(self, label, attributes):
        """
        Generate the query to extract all nodes of a label with required
        attibutes
        """
        db_query = "MATCH (n:"+ label + ") RETURN ID(n)"
        for atrribute in attributes:
            db_query = db_query + ", n." + atrribute

        return db_query


    def add_score(self, word, score):
        """
        Add score to the scorekeeper for the current query
        """
        node_list = self.countkeeper[self.matcher.get(word)]
        num_nodes = len(node_list)

        score = score / num_nodes

        for node in node_list:
            self.scorekeeper[node] = self.scorekeeper[node] + score

    def update_matcher(self):
        """
        Update the matching Automaton from the database
        """
        # Reset state
        self.scorekeeper = {}
        self.countkeeper = {}
        self.matcher = Automaton()
        # Fill data
        result = []
        cluster_no = 0
        for model in self.models:
            label_name = model['name']
            attributes = model['attributes']
            db_query = self.gen_db_query(label_name, attributes)
            # Fetch data from the DB
            result = list(connection.graph.run(db_query))

            for record in result:
                # Add key to scorekeeper
                self.scorekeeper[record[0]] = 0
                for id in range(len(attributes)):
                    if record[id + 1]:
                        for word in record[id + 1].split():
                            # Convert to lowercase
                            word = word.strip(',.').lower()
                            # Stop common filler words
                            if word in self.stoplist:
                                continue
                            if word not in self.matcher:
                                # This is a new entry
                                self.matcher.add_word(word, cluster_no)
                                self.countkeeper[cluster_no] = [record[0]]
                                cluster_no = cluster_no + 1
                            else:
                                # Update already existing entry
                                self.countkeeper[self.matcher.get(word)].append(record[0])
        return self

    def process_query(self, query):
        """
        Process input query and assign score according to the keyword frequency
        and document frequency of the keywords
        """
        # Reset the scorekeeper
        self.scorekeeper = dict.fromkeys(self.scorekeeper, 0)
        # Split the query
        keywords = query.split()
        num_keywords = 0
        # Add the score of each matching word to the scorekeeper
        for word in keywords:
            word = word.strip(",.").lower()
            head_word = word[:self.PREFIX_LIMIT]
            if head_word == '':
                continue
            if word in self.stoplist:
                continue
            num_keywords = num_keywords + 1
            match_list = self.matcher.keys(head_word + "?", "?",
                                                   MATCH_AT_LEAST_PREFIX)
            for key in match_list:
                if key == word:
                    score = self.MATCH_EXACT
                else:
                    matching_length = 0
                    min_len = min(len(key), len(word))

                    for id in range(min_len):
                        if word[id] != key[id]:
                            break
                        matching_length = matching_length + 1

                    score = self.MATCH_EXACT / (len(word) - matching_length + 1)

                self.add_score(key, score)

        return num_keywords 

    def get_candidates(self, query):
        """
        Match query keywords and return list of candidate node ids
        """
        num_keywords = self.process_query(query)
        result_nodes = list(filter(lambda s: 
                                   self.scorekeeper[s] > self.SCORE_CUTOFF, 
                                   heapq.nlargest(self.NUM_RESULTS, 
                                                  self.scorekeeper,
                                                  key=self.scorekeeper.get)))
        # Normalize score
        max_score = 1
        if result_nodes != []:
            max_score = self.scorekeeper[result_nodes[0]]

        for node in result_nodes:
            self.scorekeeper[node] = self.scorekeeper[node] * 100 / max_score
            if self.scorekeeper[node] == 100:
                self.scorekeeper[node] = self.scorekeeper[node] + \
                                         self.MATCH_EXACT

        result_nodes = list(map(
                        lambda s: 
                        (s, self.scorekeeper[s]),
                        result_nodes))
        return result_nodes

model = string_matcher()
