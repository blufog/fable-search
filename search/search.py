#!/usr/bin/env python3
# coding=utf-8

# API for the search microservice
import falcon
import logging
import json

from query_process import process
from string_matching import model as string_model

class query_resource:

    def on_get(self, req, resp):
        """Handles query sent as a get request"""
        if req.get_param("q") and req.get_param("user"):
            query_string = req.get_param("q")
            user_id = req.get_param("user")
            candidates = process(query_string, user_id)
            result = {"result_list":candidates}

        else:
            result = {'error':"Unexpected query, needs 'q' and 'user' params"}

        resp.body = json.dumps(result)

    def on_post(self, req, resp):
        """Accepts POST requests"""
        try:
            raw_json = str(req.stream.read().decode('utf-8'))
        except Exception as ex:
            raise falcon.HTTPError(falcon.HTTP_400, 'Error', ex.message)

        try:
            result_json = json.loads(raw_json, encoding='utf-8')
        except ValueError:
            raise falcon.HTTPError(falcon.HTTP_400,
                'Malformed JSON',
                'Could not decode the request body. The '
                'JSON was incorrect.')

        # We are ready to parse
        query_string = ""
        user_id = ""
        try:
            query_string = result_json['q']
            user_id = result_json['user_id']
        except KeyError:
            raise falcon.HTTPError(falcon.HTTP_400,
                'Malformed JSON',
                'JSON was incorrect. Need keys q and user_id')

        candidates = process(query_string, user_id)
        result = {"result_list":candidates}
        resp.body = json.dumps(result)


class update_resource:
    def on_get(self, req, resp):
        try:
            string_model.update_matcher()
            result = {"status":"success"}
        except:
            logging.warn("Index failed to update")
            result = {"status":"fail"}
            
        resp.body = json.dumps(result)

api = falcon.API()
api.add_route('/query', query_resource())
api.add_route('/update', update_resource())
