#!/usr/bin/env python3
# coding=utf-8

import logging

from conf import configuration as cfg
# Logging for the search microservice
class logsetter:
    def __init__(self):
        logging.basicConfig(format=cfg.LOG_FORMAT, level=cfg.LOG_LEVEL)

log = logsetter()
