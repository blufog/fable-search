#!/usr/bin/env python3
# coding=utf-8

"""
Module to handle relevance biasing in search results
"""

from db import db_instance as connection

NUM_BIAS = 5

def gen_hops_query(id1, id2):
    id1 = str(id1)
    id2 = str(id2)
    return "MATCH (a), (b), p=shortestPath((a)-[*]-(b)) WHERE ID(a) = " + id1 +\
            " AND ID(b) = " + id2 + " RETURN length(p)"

def get_hops(id1, id2):
    num_hops = -1

    if id1 != id2:
        db_query = gen_hops_query(id1, id2)
        result = list(connection.graph.run(db_query))
        if result != []:
            num_hops = result[0][0]
    else:
        num_hops = 0

    return num_hops

def score(hops):
    if hops < 0:
        return 0

    return 100/(hops + 1)

def neighborhood_sort(node_list, user_id):
    """
    Return a sorted list of nodes biased on neighboorhood from user_id
    """

    node_list[:NUM_BIAS] = list(map(lambda s: (s[0], s[1] +
                                               score(get_hops(user_id, s[0]))),
                                               node_list[:NUM_BIAS]))

    node_list.sort(key=lambda s: s[1], reverse=True)

    node_list = list(map(lambda s: s[0], node_list))

    return node_list
