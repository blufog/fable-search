#!/usr/bin/env python3
# coding=utf-8
import logging
"""
Default configuration file for the search service
""" 

class config:
    def __init__(self):
        # Sets default configuration
        # Database connection configuration
        self.DATABASE_PORT = "7687"
        self.DATABASE_HOST = "localhost"
        self.DATABASE_PROTOCOL = "bolt://"
        self.DATABASE_URI = self.DATABASE_PROTOCOL + self.DATABASE_HOST + ":" + \
                            self.DATABASE_PORT
        # Database user configuration
        self.DATABASE_USERNAME = "neo4j"
        self.DATABASE_PASSWORD = ""
        # Logging level and format
        self.LOG_LEVEL = logging.WARN
        self.LOG_FORMAT = '[%(levelname)8s] %(asctime)-15s %(message)s'

configuration = config()
