#!/usr/bin/env python3
# coding=utf-8
"""
Module to handle query processing
"""

from string_matching import model
from relevance import neighborhood_sort

def process(query_string, user_id):
    """
    Process given query and return a list of nodes
    """
    node_list = model.get_candidates(query_string)
    return neighborhood_sort(node_list, user_id)
